/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package vendor.mediatek.hardware.mtkradioex@1.0;

import android.hardware.radio@1.0::types;
import android.hardware.radio@1.0::RadioIndicationType;

/*
 * Interface declaring Digit unsolicited radio indications.
 */
interface IMwiRadioIndication {

    /**
     * Indicates the WifiMonitoringThreshouldChanged
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onWifiMonitoringThreshouldChanged(RadioIndicationType type, vec<int32_t> indStgs);

    /**
     * Indicates the WifiPdnActivate
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onWifiPdnActivate(RadioIndicationType type, vec<int32_t> indStgs);

    /**
     * Indicates the WfcPdnError
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onWfcPdnError(RadioIndicationType type, vec<int32_t> indStgs);

    /**
     * Indicates the PdnHandover
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onPdnHandover(RadioIndicationType type, vec<int32_t> indStgs);

    /**
     * Indicates the WifiRoveout
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onWifiRoveout(RadioIndicationType type, vec<string> indStgs);

    /**
     * Indicates the LocationRequest
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onLocationRequest(RadioIndicationType type, vec<string> indStgs);

    /**
     * Indicates the WfcPdnStateChanged
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onWfcPdnStateChanged(RadioIndicationType type, vec<int32_t> indStgs);

    /**
     * Indicates the NattKeepAliveChanged
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onNattKeepAliveChanged(RadioIndicationType type, vec<string> indStgs);

    /**
     * Indicates the WifiPingReques
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onWifiPingRequest(RadioIndicationType type, vec<int32_t> indStgs);

    /**
     * Indicates the onWifiPdnOOS
     *
     * @param type Type of radio indication
     * @param indStgs detail info
     */
    oneway onWifiPdnOOS(RadioIndicationType type, vec<string> indStgs);

    /**
      * Indicates the onWifiLock
      *
      * @param type Type of radio indication
      * @param indStgs detail info
      */
    oneway onWifiLock(RadioIndicationType type, vec<string> indStgs);

};
