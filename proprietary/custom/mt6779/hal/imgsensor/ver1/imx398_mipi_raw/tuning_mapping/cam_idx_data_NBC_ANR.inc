#define IDX_DATA_NBC_ANR_DIM_NS    5
#define IDX_DATA_NBC_ANR_FACTOR_SZ    4
#define IDX_DATA_NBC_ANR_ENTRY_NS    90

static unsigned int _cam_data_entry_NBC_ANR_key0000[] = {0X00000004, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_ANR_key0001[] = {0X00000004, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_ANR_key0002[] = {0X00000004, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0003[] = {0X00000008, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_ANR_key0004[] = {0X00000008, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_ANR_key0005[] = {0X00000008, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0006[] = {0X00000010, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_ANR_key0007[] = {0X00000010, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_ANR_key0008[] = {0X00000010, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0009[] = {0X00000040, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_ANR_key0010[] = {0X00000040, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_ANR_key0011[] = {0X00000040, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0012[] = {0X00000004, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_ANR_key0013[] = {0X00000004, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_ANR_key0014[] = {0X00000004, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0015[] = {0X00000008, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_ANR_key0016[] = {0X00000008, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_ANR_key0017[] = {0X00000008, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0018[] = {0X00000010, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_ANR_key0019[] = {0X00000010, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_ANR_key0020[] = {0X00000010, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0021[] = {0X00000040, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_ANR_key0022[] = {0X00000040, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_ANR_key0023[] = {0X00000040, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0024[] = {0X00000004, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0025[] = {0X00000008, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0026[] = {0X00000010, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0027[] = {0X00000040, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0028[] = {0X00000004, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0029[] = {0X00000008, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0030[] = {0X00000010, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0031[] = {0X00000040, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0032[] = {0X00000001, 0X00000000, 0X00400040, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0033[] = {0X00000001, 0X00000000, 0X00800040, 0X000007E9, };
static unsigned int _cam_data_entry_NBC_ANR_key0034[] = {0X00000001, 0X00000000, 0X01000040, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_ANR_key0035[] = {0X00000001, 0X00000000, 0X00400040, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0036[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F1, };
static unsigned int _cam_data_entry_NBC_ANR_key0037[] = {0X00000001, 0X00000000, 0X01000040, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_ANR_key0038[] = {0X00000002, 0X00000000, 0X00400080, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0039[] = {0X00000002, 0X00000000, 0X01000080, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0040[] = {0X00000000, 0X0000071C, 0XFFC1C000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0041[] = {0X00000000, 0X000038E0, 0XFFCE0000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0042[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC_ANR_key0043[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC_ANR_key0044[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0045[] = {0X01C70000, 0X00000000, 0XFFC00700, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0046[] = {0X0E380000, 0X00000000, 0XFFC03800, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0047[] = {0X30000000, 0X00000000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC_ANR_key0048[] = {0X30000000, 0X00000000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC_ANR_key0049[] = {0X30000000, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0050[] = {0X40000000, 0X00000000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC_ANR_key0051[] = {0X40000000, 0X00000000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC_ANR_key0052[] = {0X40000000, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0053[] = {0X80000000, 0X00000000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC_ANR_key0054[] = {0X80000000, 0X00000000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC_ANR_key0055[] = {0X80000000, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0056[] = {0X00000000, 0X00000002, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC_ANR_key0057[] = {0X00000000, 0X00000002, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC_ANR_key0058[] = {0X00000000, 0X00000002, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0059[] = {0X00000300, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0060[] = {0X00000C00, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_ANR_key0061[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000619, };
static unsigned int _cam_data_entry_NBC_ANR_key0062[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000039, };
static unsigned int _cam_data_entry_NBC_ANR_key0063[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X000007F9, };
static unsigned int _cam_data_entry_NBC_ANR_key0064[] = {0X0000F004, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_ANR_key0065[] = {0X0000F004, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_ANR_key0066[] = {0X0000F004, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_ANR_key0067[] = {0X00000008, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_ANR_key0068[] = {0X00000008, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_ANR_key0069[] = {0X00000008, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_ANR_key0070[] = {0X00000010, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_ANR_key0071[] = {0X00000010, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_ANR_key0072[] = {0X00000010, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_ANR_key0073[] = {0X00000040, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_ANR_key0074[] = {0X00000040, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_ANR_key0075[] = {0X00000040, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_ANR_key0076[] = {0X30000000, 0X0000C000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_ANR_key0077[] = {0X30000000, 0X0000C000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_ANR_key0078[] = {0X30000000, 0X0000C000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_ANR_key0079[] = {0X40000000, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_ANR_key0080[] = {0X40000000, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_ANR_key0081[] = {0X40000000, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_ANR_key0082[] = {0X80000000, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_ANR_key0083[] = {0X80000000, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_ANR_key0084[] = {0X80000000, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_ANR_key0085[] = {0X00000000, 0X00000002, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_ANR_key0086[] = {0X00000000, 0X00000002, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_ANR_key0087[] = {0X00000000, 0X00000002, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_ANR_key0088[] = {0X00000001, 0X00000000, 0X00800040, 0X000007EA, };
static unsigned int _cam_data_entry_NBC_ANR_key0089[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F2, };

static IDX_MASK_ENTRY _cam_data_entry_NBC_ANR[IDX_DATA_NBC_ANR_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0000, 0, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0001, 11, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0002, 1, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0003, 12, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0004, 23, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0005, 13, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0006, 24, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0007, 35, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0008, 25, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0009, 36, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0010, 47, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0011, 37, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0012, 0, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0013, 11, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0014, 1, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0015, 12, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0016, 23, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0017, 13, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0018, 24, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0019, 35, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0020, 25, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0021, 36, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0022, 47, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0023, 37, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0024, 48, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0025, 56, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0026, 64, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0027, 72, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0028, 48, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0029, 56, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0030, 64, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0031, 72, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0032, 80, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0033, 80, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0034, 80, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0035, 80, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0036, 80, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0037, 80, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0038, 88, 10, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0039, 88, 11, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0040, 80, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0041, 88, 13, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0042, 0, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0043, 11, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0044, 1, 14, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0045, 80, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0046, 88, 16, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0047, 0, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0048, 11, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0049, 1, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0050, 12, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0051, 23, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0052, 13, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0053, 24, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0054, 35, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0055, 25, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0056, 36, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0057, 47, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0058, 37, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0059, 80, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0060, 88, 19, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0061, 0, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0062, 11, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0063, 1, 20, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0064, 0, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0065, 11, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0066, 1, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0067, 12, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0068, 23, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0069, 13, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0070, 24, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0071, 35, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0072, 25, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0073, 36, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0074, 47, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0075, 37, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0076, 0, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0077, 11, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0078, 1, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0079, 12, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0080, 23, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0081, 13, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0082, 24, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0083, 35, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0084, 25, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0085, 36, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0086, 47, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0087, 37, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0088, 80, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_ANR_key0089, 80, 24, 0, 0},
};

static unsigned short _cam_data_dims_NBC_ANR[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_LV,
};

static unsigned short _cam_data_expand_NBC_ANR[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_NBC_ANR =
{
    {IDX_ALGO_MASK, IDX_DATA_NBC_ANR_DIM_NS, (unsigned short*)&_cam_data_dims_NBC_ANR, (unsigned short*)&_cam_data_expand_NBC_ANR},
    {IDX_DATA_NBC_ANR_ENTRY_NS, IDX_DATA_NBC_ANR_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_NBC_ANR}
};