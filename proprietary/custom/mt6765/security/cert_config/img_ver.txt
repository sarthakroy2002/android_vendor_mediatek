[lk]
img_ver = 0
img_group = 0

[spmfw]
img_ver = 0
img_group = 1

[logo]
img_ver = 0
img_group = 1

[boot]
img_ver = 0
img_group = 1

[recovery]
img_ver = 0
img_group = 1

[dtbo]
img_ver = 0
img_group = 1

[atf]
img_ver = 0
img_group = 0

[atf_dram]
img_ver = 0
img_group = 0

[tee]
img_ver = 0
img_group = 0

[md1drdi]
img_ver = 0
img_group = 1

[md1dsp]
img_ver = 0
img_group = 1

[md1rom]
img_ver = 0
img_group = 1

[tinysys-sspm]
img_ver = 0
img_group = 0

[tinysys-loader-CM4_A]
img_ver = 0
img_group = 1

[tinysys-scp-CM4_A]
img_ver = 0
img_group = 1

[tinysys-scp-CM4_A_dram]
img_ver = 0
img_group = 1

[recovery-ramdisk]
img_ver = 0
img_group = 1

[recovery-vendor]
img_ver = 0
img_group = 1

[dconfig]
img_ver = 0
img_group = 1

[dconfig-dt]
img_ver = 0
img_group = 1

[lk_main_dtb]
img_ver = 0
img_group = 1

[gz]
img_ver = 0
img_group = 0

[vm]
img_ver = 0
img_group = 0
